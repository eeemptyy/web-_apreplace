<?php
   require 'db_controller.php';

    /* this file will act as a gate to db_controller on ajax called */ 

    $db_controller = new DB_Controller();

    $case_sw = $_POST['func'];
   
    switch ($case_sw) {
        case "get_modelData":
            $model_num = $_POST['selected'];
            echo $db_controller->get_modelData($model_num);
            break;
        case "get_modelEosTable":
            echo $db_controllerobj->get_modelEosTable();
            break;
        case "getLocationTable":
            $model_num = $_POST['selected'];
            echo $db_controller->getLocationTable($model_num);
            break;
        case "getIpTable":
            $model_num = $_POST['model'];
            $facname = $_POST['fac'];
            echo $db_controller->getIpTable($model_num, $facname);
            break;
        case "update_database":
            $modelname = $_POST['model'];
            $eol = $_POST['eol'];
            $eos = $_POST['eos'];
            $picname = $_POST['picname'];
            echo $db_controller->update_database($modelname, $eol, $eos, $picname);
            break;
        default:
            echo "Function Not Found.";
}

?>