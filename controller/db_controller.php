<?php

class DB_Controller{
    
    private $username = "";
    private $password = "";
    private $hostname = "";
    private $dbname = "";   

    private $connection;
    private $modelMap = array();
    private $modelNumberArr = array();
    private $facultyMap = array();
    private $outDrop= "";
    // private $highchartData = array();
    
    public function __construct(){
        include("dbconfig.php");
        $this->username = $uname;
        $this->password = $pass;
        $this->hostname = $host;
        $this->dbname = $db;
        
        try{
            
            $this->connection = new PDO("mysql:host={$this->hostname};"."dbname={$this->dbname}",$this->username,$this->password);
            // $this->connection->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
            
        } catch (PDOException $e){
            die("Couldn't connect to the database ".$dbname.": ".$e->getMessage());
        }

        // Init data.
        $this->createModelMap(); // create modelmap to map same model number with the long name.
        $this->createmodelNumberArrOutdrop(); // create array of model number with no duplicate and create dropdown Item.
        $this->createFacultyMap(); // create FacultyMap to map a fac_id and fac name use for query faculty and model of ap.
    }

    private function createFacultyMap(){
        /* This function will create a Map between fac_id and fac
           read from database and send to facultyMap */
        try {
            $sql = 'select fac, fac_id from faculty';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n=1;
        while ($row = $q->fetch()){
            $test = "In while".$n;
            $facname = htmlspecialchars(iconv("tis-620", "utf-8",$row['fac']));
            $facname = preg_replace("/ /", "", $facname);
            $facId = $row['fac_id'];
            $this->facultyMap[$facname] = $facId;
            $n++;
        }
        asort($this->facultyMap);
    }

    private function createModelMap(){
        /* This function must! call before createmodelNumberArrOutdrop function
           this will create an array of all accesspoint model from database
           and send to modelMap */
        try {
            $sql = 'select * from lifeapste';
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $n=1;
        while ($row = $q->fetch()){
            $test = "In while".$n;
            $modelname = htmlspecialchars($row['model']);
            $temp = explode("-", $modelname);
            $modelnumber = preg_replace("/[A-Z]/", "", $temp[1]);
            $this->modelMap[$modelname] = $modelnumber;
            $n++;
        }
        asort($this->modelMap);
    }

    private function createmodelNumberArrOutdrop(){
        /* This function use data from  createModelMap function to 
            remove duplicate model and normalize model name to only number of model
            and save modelnumber with no duplicate to outDrop function*/
        foreach ($this->modelMap as $key => $value) {
            $test = "In Foreach22";
            if (!in_array($value, $this->modelNumberArr)){
                $this->modelNumberArr[] = $value;
            }
            sort($this->modelNumberArr);
        }
        foreach ($this->modelNumberArr as $value){
            $test = "In foreach";
            $this->outDrop .= "<option>$value</option>";
        }
    }

    public function writeHighchartsTableFile(){
        /* This function will create two html files(eostable.html, eoltable.html)
            as a preprocess for highcharts in repart.php. this function will call
            everytimes when report.php call (call in get_modelEosTable function) */
        $model_number = $this->modelNumberArr;
        $highchartData = array();
        foreach ($model_number as $value) {
            if ($value == "702"){
                $value = "P702";
            }
            try {
                $sql = "SELECT infoap.model, lifeapste.eol, lifeapste.eos, COUNT(infoap.model) as TotalAP ".
                    "FROM infoap ".
                    "INNER JOIN lifeapste ".
                    "ON infoap.model=lifeapste.model ".
                    "WHERE infoap.model like '%".$value."%'";
                $q = $this->connection->prepare($sql);
                $q->execute();
                $row = $q->fetch();
            } catch (Exception $e) {
                die($e->getMessage());
            }
            if ($value == "P702"){
                $value = "702";
            }
            $highchartdataTemp = array("model"=>$value, "eol"=>$row['eol'],
                                        "eos"=>$row['eos'], "amount"=>$row['TotalAP']);
            $highchartData[] = $highchartdataTemp;
        }
        $myfile = fopen("temptable/eoltable.html", "w") or die("Unable to open file!");
        $myfileEos = fopen("temptable/eostable.html", "w") or die("Unable to open file!");
        $txt = "";
        $txtEos = "";
        $highchartEolColHeader = array();
        $highchartEosColHeader = array();
        $highchartEolRowHeader = array();
        $highchartEosRowHeader = array();
        foreach ($highchartData as $arr){
            $year = explode("-", $arr['eol']);
            $yearEos = explode("-", $arr['eos']);
            if ($year[0] != "0000"){
                $highchartEolColHeader[] = $arr['model'];
                if (!in_array($year[0], $highchartEolRowHeader)){
                    $highchartEolRowHeader[] = $year[0];   
                }
            }
            if ($yearEos[0] != "0000"){
                $highchartEosColHeader[] = $arr['model'];
                if (!in_array($yearEos[0], $highchartEosRowHeader)){
                    $highchartEosRowHeader[] = $yearEos[0];   
                }
            }
        }
        asort($highchartEolRowHeader);
        asort($highchartEosRowHeader);
        
/*
    Start creating data fot EOL Chart
*/
        $txt .= "<table id='highchart_eol1' style='display:none;'>"."<thead><tr>";
        $txt .= "<th></th>";
        foreach ($highchartEolColHeader as $key => $value) {
            $txt .= "<th>".$value."</th>";
        }
        $txt .= "</tr></thead><tbody>";
        foreach ($highchartEolRowHeader as $key => $year) {
            $txt .= "<tr><th><p>".$year."</p></th>";
            foreach ($highchartData as $arr) {
                if (in_array($arr['model'], $highchartEolColHeader)){
                    $tempYear = explode("-", $arr['eol']);
                    if ($tempYear[0] == $year){
                        $txt .= "<td>".$arr['amount']."</td>";                    
                    }else {
                        $txt .= "<td></td>";
                    }
                }
            }
            $txt .= "</tr>";
        }
        $txt .= "</tbody></table>";
/*
    Data for EOL Chart created

    Start creating data fot EOS Chart
*/
        $txtEos .= "<table id='highchart_eos1' style='display:none;'>"."<thead><tr>";
        $txtEos .= "<th></th>";
        foreach ($highchartEosColHeader as $key => $value) {
            $txtEos .= "<th>".$value."</th>";
        }
        $txtEos .= "</tr></thead><tbody>";
        foreach ($highchartEosRowHeader as $key => $year) {
            $txtEos .= "<tr><th><p>".$year."</p></th>";
            foreach ($highchartData as $arr) {
                if (in_array($arr['model'], $highchartEosColHeader)){
                    $tempYear = explode("-", $arr['eos']);
                    if ($tempYear[0] == $year){
                        $txtEos .= "<td>".$arr['amount']."</td>";                    
                    }else {
                        $txtEos .= "<td></td>";
                    }
                }
            }
            $txtEos .= "</tr>";
        }
        $txtEos .= "</tbody></table>";
/*
    Data for EOL Chart created
*/
        fwrite($myfile, $txt);
        fclose($myfile);
        fwrite($myfileEos, $txtEos);
        fclose($myfileEos);
        
    }

    public function get_dropdownModel(){
        /* send data to dropdown menu in index.php */
        return $this->outDrop;
    }

    public function get_modelData($model_in){
        /* this function return a details of ap model user selected in json form */
        $model = $model_in;
        $model_location = $model_in;
        $modelData=array();

        foreach ($this->modelMap as $key => $value) {
            // $modelData .= "|IN LOOP|++";
            if ($model == $value){
            $model = $key;
            break;
            }
        }
        try {
            $sql = "select * from lifeapste where model = '".$model."'";
            $q = $this->connection->prepare($sql);
            // $q->setFetchMode(PDO::FETCH_ASSOC);
            $q->execute();
            $row = $q->fetch();
            $modelData['eol'] = $row['eol'];
            $modelData['eos'] = $row['eos'];
            $modelData['picname'] = $row['picname'];
        } catch (Exception $e) {
            die($e->getMessage());
        }

        try {
            // $sql = "SELECT model, Location FROM infoap WHERE model like '%".$model_location."%'";
            $sql = "SELECT infoap.model, infoap.Location, infoap.fac_id, infoap.cl_max ". 
                    "FROM infoap ".
                    "WHERE infoap.model like '%".$model_location."%' ".
                    "GROUP BY infoap.fac_id";
            $q = $this->connection->query($sql);
            $q->setFetchMode(PDO::FETCH_ASSOC);
        } catch (Exception $e) {
            die($e->getMessage());
        }
        $temp = "";$n=1;
        while ($row = $q->fetch()){
            // $temp .= iconv("tis-620","utf-8",$row['Location']).", ";
            $key = array_search($row['fac_id'], $this->facultyMap);
            $temp .= $n." ".$key."<BR> ";
            $n++;
        }
        $modelData['Location'] = $temp;
        return json_encode($modelData);

    }

    public function get_modelEosTable(){

        /* this function create a modelEosTable(Table 1 on the top)
           in html table form sending via ajax */

        $model_number = $this->modelNumberArr;
        $table_item = "<tbody>";
        foreach ($model_number as $value) {
            if ($value == "702"){
                $value = "P702";
            }
            try {
                $sql = "SELECT infoap.model, lifeapste.eol, lifeapste.eos, COUNT(infoap.model) as TotalAP ".
                    "FROM infoap ".
                    "INNER JOIN lifeapste ".
                    "ON infoap.model=lifeapste.model ".
                    "WHERE infoap.model like '%".$value."%'";
                $q = $this->connection->prepare($sql);
                $q->execute();
                $row = $q->fetch();
            } catch (Exception $e) {
                die($e->getMessage());
            }
            
            if ($value == "P702"){
                $value = "702";
            }
            if (($row['eol'] != '0000-00-00') OR ($row['eos'] != 0000-00-00)) {
                $table_item .= "<tr>".
                                "<td class=color_link><a href='#fac_content' style='cursor: pointer;' onclick=getfac_table($value)>Cisco Aironet ".$value."</a></td>".
                                "<td>".$row['eol']."</td>".
                                "<td>".$row['eos']."</td>".
                                "<td>".$row['TotalAP']."</td></tr>";
            }
        }
        $table_item .= "</tbody>";
        $this->writeHighchartsTableFile();
        return $table_item;
    }

    public function getLocationTable($modelNum){
        /* this function create a facultyTable(Table 2 the location of model selected)
           in html table form sending via ajax */

        if ($modelNum == "default"){
            return "<tbody><tr>".
                    "<td> </td>".
                    "<td>No model</td>".
                    "<td> Selected </td>".
                    "<td> </td>".
                    "<td> </td></tr>".
                    "</tbody>";
        }else {
            try {

                if ($modelNum == "702"){
                    $modelNum = "P702";
                }

                $table_item = "<tbody>";
                $sql =    "SELECT fac_id, fac, COUNT( * ) AS num_ap, SUM(IF(a.cl_max>34,1,0))as ap_replace, SUM(a.cl_max)as cl_max 
                FROM  `infoap` a 
                JOIN faculty USING ( fac_id ) 
                WHERE model like '%".$modelNum."%' GROUP BY fac_id ORDER BY cl_max DESC"; 
                
                if ($modelNum == "P702"){
                    $modelNum = "702";
                }

                $q = $this->connection->query($sql);
                $q->setFetchMode(PDO::FETCH_ASSOC);
                while ($row = $q->fetch()) {
                    $fac_temp = htmlspecialchars(iconv("tis-620", "utf-8",$row['fac']));
                    $fac_temp = preg_replace("/ /", "", $fac_temp);
                    $table_item .= "<tr>".
                                        "<td>".$row['fac_id']."</td>".
                                        "<td><a style='cursor: pointer;' onclick=getfacDetailsTable('".$fac_temp."');>$fac_temp</a></td>".
                                        "<td>".$row['num_ap']."</td>".
                                        "<td>".$row['ap_replace']."</td>".
                                        "<td>".$row['cl_max']."</td></tr>";
                }
                $table_item .= "</tbody>";

            } catch (Exception $e) {
                die($e->getMessage());
            }
            return $table_item;
        }
    }

    public function getIpTable($modelNum, $facname){
        /* this function create a getIpTable(Table 3 IP of each ap in the
           Location selected) in html table form sending via ajax */

        if ($modelNum == "default"){
            return "<tbody><tr>".
                    "<td> </td>".
                    "<td>No Location</td>".
                    "<td> Selected </td>".
                    "<td></td>".
                    "</tr>".
                    "</tbody>";
        }else {
            if ($modelNum == "702"){
                $modelNum = "P702";
            }
            try {
                $sql = "SELECT infoap.IP, infoap.install_time, infoap.Location, infoap.cl_max ".
                        "FROM infoap ".
                        "WHERE infoap.model like '%".$modelNum."%' and infoap.fac_id = ".$this->facultyMap[$facname];
                $q = $this->connection->prepare($sql);
                $q->execute();
                // $row = $q->fetch();
            } catch (Exception $e) {
                die($e->getMessage());
            }    

            if ($modelNum == "P702"){
                $modelNum = "702";
            }
            $table_item = "<tbody>";
            while ($row = $q->fetch()) {
                $table_item .= "<tr>".
                                    "<td>".$row['IP']."</td>".
                                    "<td>".$row['install_time']."</td>".
                                    "<td>".iconv("tis-620", "utf-8",$row['Location'])."</td>".
                                    "<td>".$row['cl_max']."</td></tr>";
            }
            $table_item .= "</tbody>";
            return $table_item;
        }
    }

    public function update_database($modelname, $eol, $eos, $picname){
        /* Preprocess to update the database after user submit form */
        if (($modelname == "Choose Model") || ($modelname == "")){
            die("Wrong selected model name is: ".$modelname);
        }
        
        if ($modelname == "702"){
                $modelname = "P702";
        }
        foreach ($this->modelMap as $key => $value) {
            $pattern = "/.*$modelname.*/";
            if (preg_match($pattern, $key)){
                $model = $key;
                $this->makeUpdate($model, $eol, $eos, $picname);
            }
        }
        if ($modelname == "P702"){
                $modelname = "702";
        }        
        return "Update Successfulty!";
    }

    public function makeUpdate($model, $eol, $eos, $picname){
        /* update database only call by update_database function */

        try {
            $sql = "UPDATE lifeapste SET eol = '".$eol."', eos = '".$eos."', picname = '".$picname."' WHERE lifeapste.model = '".$model."'";
            $q = $this->connection->prepare($sql);
            $q->execute();

        } catch (Exception $e) {
            die($e->getMessage());
        }
    }

}


?>