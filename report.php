<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="A Web application during trainee in OSC Kasetsart University">
    <meta name="author" content="Jompol Sermsook, Thanks to startbootstrap for css framework, Thanks.">

    <title>KU Access Point Replacement - Report</title>
    
    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    

<?php
    include("temptable/eoltable.html");
    include("temptable/eostable.html");
    require 'controller/db_controller.php';
    $controller = new DB_Controller();
?>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#">OCS Access Point Replacement</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li >
                        <a href="index.php">Edit</a>
                    </li>
                    <li >
                        <a href="#report_content">Report</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                    <!--<li>
                         <a href="http://kuwin.ku.ac.th/">Sign out</a>
                    </li>-->
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Page Content -->

    <div id="report_content" class="content-section-a" style="display:block;">

        <h2 style="text-align:center;">ACCESS POINT DATA REPORT</h2>
        <hr class="intro-divider"><BR><BR>
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <h4>Model Table</h4>
                    <table id="eoleos_table" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Model Number&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th>
                            <th>End of Life</th>
                            <th>End of Service</th>
                            <th>Amount</th>
                        </tr>
                    </thead>
                    <?php echo $controller->get_modelEosTable(); ?>
                    </table>
                </div>
                <BR>
                 <div class="btn-group pull-right">
                    <button id="eolChartBtt" type="button" onclick="eolBtt();" class="btn btn-primary">End of Life</button>
                    <button id="eosChartBtt" type="button" onclick="eosBtt();" class="btn btn-primary">End of Service</button>
                </div>
                <BR><BR>
                <div id="container" class="col-lg-6 col-sm-6" align="center" style="margin:auto;border-style: solid;"></div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <div id="fac_content" class="content-section-b" style="display:block;">

        <h3 id="model_header_table" style="text-align:center;">No Model Selected</h3>
        
        <hr class="intro-divider">
        <div class="row">
            <div class="col-lg-6 col-sm-6">
                <img id="picture" class="img-responsive" src="img/ap/noimage.jpg"  style="display: block;margin: 0 auto;" 
                    alt="Access point Images" onclick="window.open(this.src);" onerror="this.src='img/ap/noimage.jpg'">
            </div>
            <div class="col-lg-6 col-sm-6">
            <BR><BR>
                <h4 id='eol_input'>End of Life: </h4>
                <h4 id='eos_input'>End of Service: </h4>
            </div>
        </div>
        <BR>           
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-sm-6">
                    <div>
                    <table id="locationTable" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Fac_id</th>
                            <th>Fac</th>
                            <th>num_ap</th>
                            <th>ap_replace</th>
                            <th>cl_max</th>
                        </tr>
                    </thead>
                    <?php echo  $controller->getLocationTable("default");?>
                    </table>  
                    </div>
                </div>
                <div class="col-lg-6 col-sm-6">
                    <div>
                    <table id="iptable" class="table table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>IP Address</th>
                            <th>InstalledTime</th>
                            <th>Location</th>
                            <th>cl_max</th>
                        </tr>
                    </thead>
                    <?php echo  $controller->getIpTable("default", "");?>
                    </table>  
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->



    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row" align="center">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="http://kuwin.ku.ac.th/">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="index.php">Edit</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#report_content" onclick="goReport();">Report</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="contact.html">Contact</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small" style="text-align:center;opacity: 0.5;">Copyright &copy; 2016 สำนักบริการคอมพิวเตอร์ มหาวิทยาลัยเกษตรศาสตร์ |
                                                          Office of Computer Services, Kasetsart University  All Rights Reserved<BR>
                        Edited by: <a style="color:black;" title="Mail to editor" href="mailto: jompol.s@ku.th" target="_blank">Jompol Sermsook</a>
                        | Theme Designed by: Start Bootstrap Project | 
                        Thanks to <a style="color:black;" title="StartBootstrap.com" href="http://startbootstrap.com/" target="_blank">Startbootstrap.com</a>,
                        maintained by <a style="color:black;" title="David Miller" href="http://davidmiller.io/" target="_blank">David Miller</a>
                        at <a style="color:black;" title="Blackrock Digital" href="http://blackrockdigital.io/" target="_blank">Blackrock Digital</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom JavaScript Function -->
    <script src="js/custom_script.js"></script>

    <!-- dataTable -->
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>
    
    <!-- Highcharts Custom JavaScript -->
    <script src="js/highcharts-custom.js"></script>

    <script>
        var contains = 'highchart_eol1';
        var titleText = 'Access Point End of Life';
        function eolBtt(){
            contains = 'highchart_eol1';
            titleText = 'Access Point End of Life';
            $("#eolChartBtt").css("background-color","#00004d");
            $("#eosChartBtt").css("background-color","#337ab7");
            drawCharts();
        }

        function eosBtt(){
            contains = 'highchart_eos1';
            titleText = 'Access Point End of Service';
            $("#eolChartBtt").css("background-color","#337ab7");
            $("#eosChartBtt").css("background-color","#00004d");
            drawCharts();
        }

        function drawCharts(){
            // alert("drawCharts");
            $('#container').highcharts({
                data: {
                    table: contains
                },
                chart: {
                    type: 'column'
                },
                title: {
                    text: titleText
                },
                yAxis: {
                    allowDecimals: false,
                    title: {
                        text: 'Units'
                    }
                },
                tooltip: {
                    formatter: function () {
                        return '<b>Cisco Aironet: ' + this.series.name + '</b><br/>' +
                            '<b>Amount: ' +this.point.y + '</b><br/>' + 
                            '<b>End in: ' +this.point.name.toLowerCase() + '</b><br/>';
                    }
                }
            });
        }
        
        $(function () {
            drawCharts();
        });

    </script>

</body>

</html>
