
 <?php
    // session_start();
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="KU Access Point Replacement for OSC Kasetsart University">
    <meta name="author" content="Jompol Sermsook, Thanks to startbootstrap for css framework.">

    <title>KU Access Point Replacement - Edit</title>

    <!-- DataTables CSS -->
    <link rel="stylesheet" type="text/css" href="DataTables/datatables.min.css"/>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Datepicker CSS -->
    <link href="css/datepicker.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/landing-page.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->


<style>
.fileUpload {
    position: relative;
    overflow: hidden;
    width: 100px;
    /*margin: 10px;*/
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
<?php
    // if(session_is_registered("valid_user")&&session_is_registered("valid_level")){
    //     $user = $valid_user; $level = $valid_level;
    // }
    // if( isset($user)  and ($level >= 2)){
    //     //do something
    // }else {
    //     header("Location: report.php"); /* Redirect browser */
    //     exit();
    // }

?>

<?php
    require 'controller/db_controller.php';
    $controller = new DB_Controller();
?>

</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-default navbar-fixed-top topnav" role="navigation">
        <div class="container topnav">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand topnav" href="#">OCS Access Point Replacement</a>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li >
                        <a href="#edit_content">Edit</a>
                    </li>
                    <li >
                        <a href="report.php">Report</a>
                    </li>
                    <li>
                        <a href="contact.html">Contact</a>
                    </li>
                    <li>
                        <a href="http://kuwin.ku.ac.th/pro/admin/logout.php">Logout</a>
                    </li>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>

    <!-- Header -->
    
    <div class="intro-header">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1>KU Access Point <br>Replacement</h1>
                        <h3>Get to know All details about access point in KU</h3>
                        <hr class="intro-divider">
                        <ul class="list-inline intro-social-buttons">
                            <li>
                                <a href="#edit_content" class="btn btn-default btn-lg">&nbsp;&nbsp;&nbsp;<i class="fa fa-pencil-square-o" aria-hidden="true"></i> <span class="network-name">EDIT&nbsp;&nbsp;&nbsp;</span></a>
                            </li>
                            <li>
                                <a href="report.php" class="btn btn-default btn-lg"><i class="fa fa-table" aria-hidden="true"></i> <span class="network-name">REPORT</span></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.intro-header -->

    <!-- Page Content -->


    <div id="edit_content" class="content-section-a" style="display:block;">

        <h2 style="text-align:center;">EDIT ACCESS POINT DATA</h2>
        <hr class="intro-divider"><BR><BR>
        <div class="container">
            <div class="row">
                <div class="col-lg-5 col-sm-6">
                    <div class="clearfix"></div>
                     <form role="form">
                        <h4>Details</h4><BR>
                        <div class="form-group">
                            <label for="model_lb">Model Number</label>
                            <select name="model_drop" id="model_drop" onchange="get_modelData();">
                                <option>Choose Model</option>
                                <?php echo $controller->get_dropdownModel(); ?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="eol_lb">End of Life</label>
                            <input type="text" class="datepicker form-control" id="eol_input">
                            <!--<input type="date" class="form-control" id="eol_input">-->
                        </div>
                        <div class="form-group">
                            <label for="eos_lb">End of Service</label>
                            <input type="text" class="datepicker form-control" id="eos_input">
                            <!--<input type="date" class="form-control" id="eos_input">-->
                        </div>
                        <div class="form-group">
                        <label for="picture_lb">Picture</label>
                            <div class="row">
                            
                                <div class="col-sm-5">
                                    <input type="text" class="form-control" id="picture_name_input">                                
                                </div>
                                <div class="col-sm-3">
                                    <button id="upload_Btt" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal">Upload</button>
                                </div>
                            </div>
                        </div>
                        <button id="submit_edit_btn" onclick="submitForm();" type="button" class="btn btn-default" style="display:block;">Submit</button>
                      <!--  <input data-provide="datepicker" class="datepicker" id="dp2" type="text">
                        <input class="datepicker" type="text"> -->
                    </form>
                </div>

                <div class="col-lg-5 col-lg-offset-2 col-sm-6">
                    <h4 id="model_number_head">Model</h4>
                    <img id="picture" class="img-responsive" src="img/ap/noimage.jpg"  style="display: block;margin: 0 auto;" 
                    alt="Access point Images" onclick="window.open(this.src);" onerror="this.src='img/ap/noimage.jpg'">
                    <h4>Location</h4>
                    <p id="location_output" style="height:100px;border:1px solid #ccc;overflow:auto;">
                        All the Location of the selected access point will be shown here when you choose
                        the Model Number.
                    </p>
                
                </div>
            </div>

        </div>
        <!-- /.container -->

    </div>
    <!-- /.content-section-a -->

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row" align="center">
                <div class="col-lg-12">
                    <ul class="list-inline">
                        <li>
                            <a href="http://kuwin.ku.ac.th/">Home</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="#edit_content">Edit</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="report.php">Report</a>
                        </li>
                        <li class="footer-menu-divider">&sdot;</li>
                        <li>
                            <a href="contact.html">Contact</a>
                        </li>
                    </ul>
                    <p class="copyright text-muted small" style="text-align:center;opacity: 0.5;">Copyright &copy; 2016 สำนักบริการคอมพิวเตอร์ มหาวิทยาลัยเกษตรศาสตร์ |
                                                          Office of Computer Services, Kasetsart University  All Rights Reserved<BR>
                        Edited by: <a style="color:black;" title="Mail to editor" href="mailto: jompol.s@ku.th" target="_blank">Jompol Sermsook</a>
                        | Theme Designed by: Start Bootstrap Project | 
                        Thanks to <a style="color:black;" title="StartBootstrap.com" href="http://startbootstrap.com/" target="_blank">Startbootstrap.com</a>,
                        maintained by <a style="color:black;" title="David Miller" href="http://davidmiller.io/" target="_blank">David Miller</a>
                        at <a style="color:black;" title="Blackrock Digital" href="http://blackrockdigital.io/" target="_blank">Blackrock Digital</a>
                    </p>
                </div>
            </div>
        </div>
    </footer>
    

    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="closeModalBtt" type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Upload model picture</h4>
                </div>
                <div class="modal-body">
                <form id="modol_form" role="form" action="controller/upload_ap_picture.php" method="post" enctype="multipart/form-data" target="_blank">                       
                    <p id="file_data" align="center">
                        File Data.<BR>
                        Name:<BR>
                        Size:<BR>
                        Type:<BR>
                    </p>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="fileUpload btn btn-primary">
                                <span>Browse</span>
                                <input type="file" name="fileToUpload" class="upload" onchange="handleFiles(this.files)" accept=".jpg">
                            </div>                                
                        </div>
                        <div class="col-sm-5">
                            <input type="text" class="form-control" id="picture_name">                                
                        </div>
                    </div>
                </form>
                </div>
                <div class="modal-footer">
                    <button type="button" onclick="modalClose()" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    
    <!-- jQuery Datepicker -->
    <script src="js/bootstrap-date.js"></script>
    
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom JavaScript Function -->
    <script src="js/custom_script.js"></script>

    <script>
        $('.datepicker').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true,
        });
    </script>

    <!-- dataTable -->
    <script type="text/javascript" src="DataTables/datatables.min.js"></script>

    <script>
        $(document).ready(function(){
           	document.getElementById('upload_Btt').disabled = true;
            document.getElementById('submit_edit_btn').disabled = true; 
        });
    </script>

</body>

</html>
