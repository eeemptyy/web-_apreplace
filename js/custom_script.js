function get_modelData(){

    var model_selected = $('#model_drop').val();

    if (model_selected == "Choose Model"){
        
        document.getElementById('upload_Btt').disabled = true;
        document.getElementById('submit_edit_btn').disabled = true;
        alert("Please choose model number");
        $('#model_number_head').html("Model");
        $('#eol_input').val("");
        $('#eos_input').val("");
        $('#picture_name_input').val("");
        $('#location_output').html("");
        $('#picture').attr("src", "img/ap/noimage.jpg");
                        
    }else {
            
        document.getElementById('upload_Btt').disabled = false;
        document.getElementById('submit_edit_btn').disabled = false; 

        $('#model_number_head').html("Cisco Aironet "+model_selected);
        $.ajax({
            type: "POST",
            url: "controller/phpfunction_switcher.php",
            data: {
                    func: "get_modelData",
                    selected: model_selected
                  },
            success: function(data) {
                var obj = JSON.parse(data);
                $('#eol_input').val(obj['eol']);
                $('#eos_input').val(obj['eos']);
                $('#picture_name_input').val(obj['picname']);
                $('#location_output').html(obj['Location']);
                $('#picture').attr("src", "img/ap/"+ obj['picname']);
            },
            error: function(data) {
                alert("error "+data);
            }
        });
    }
}

function get_modelDataForReport(){
    var temp = $('#model_header_table').text().split(" ");
    var modelNum = temp[2];
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
                func: "get_modelData",
                selected: modelNum
            },
        success: function(data) {
            var obj = JSON.parse(data);
            $('#eol_input').html("End of Life:&nbsp;&nbsp;&nbsp;"+obj['eol']);
            $('#eos_input').html("End of Service:&nbsp;&nbsp;&nbsp;"+obj['eos']);
            $('#picture').attr("src", "img/ap/"+ obj['picname']);
        },
        error: function(data) {
            alert("error "+data);
        }
    });
}

function getfac_table(value){
    $('#model_header_table').html("Cisco Aironet "+value);
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
                func: "getLocationTable",
                selected: value
              },
        success: function(data) {
            var outTable = "<table id='locationTable' class='table table-hover table-condensed'>"+
                            "<thead><tr>"+
                            "<th>Fac_id</th>"+
                            "<th>Fac</th>"+
                            "<th>num_ap</th>"+
                            "<th>ap_replace</th>"+
                            "<th>cl_max</th>"+
                            "</tr>"+
                            "</thead>"+
                            data +
                            "</table>";
            $('#locationTable').html(outTable);
            $('#locationTable').DataTable({
                "order": [[ 4, "desc" ]], 
                "pageLength": 10,
                destroy: true,
                "autoWidth": false
            });
        },
        error: function(data) {
            alert("error "+data);
        }
    });
    get_modelDataForReport();
}
function getfacDetailsTable(facname){
    var temp = $('#model_header_table').text().split(" ");
    var modelNum = temp[2];
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
                func: "getIpTable",
                model: modelNum,
                fac: facname
              },
        success: function(data) {
            var outTable = "<table id='iptable' class='table table-hover table-condensed'>"+
                            "<thead><tr>"+
                            "<th>Fac_id</th>"+
                            "<th>Model</th>"+
                            "<th>Location</th>"+
                            "<th>cl_max</th>"+
                            "</tr>"+
                            "</thead>"+
                            data +
                            "</table>";
            $('#iptable').html(outTable);
            $('#iptable').DataTable({
                "order": [[ 3, "desc" ]],
                "pageLength": 10,
                destroy: true,
                "autoWidth": false
            });
        },
        error: function(data) {
            alert("error "+data);
        }
    });
}

function handleFiles(files){
    
    var file = files[0];
    var reader = new FileReader();
    reader.readAsText(file);
    // alert("<------ From file ------->"+
    //         "\nFile name: "+file.name+
    //         "\nFile size: "+file.size+
    //         "\nFile type: "+file.type
    // );
    if (file.name == null){
        var picname = "noimage.jpg";
    }else {
        var picname = file.name;
    }
    document.getElementById("file_data").innerHTML = "File Data.<BR>"+
    "Name: "+file.name+"<BR>"+
    "Size: "+file.size+"<BR>"+
    "Type: "+file.type+"<BR>";
    document.getElementById("picture_name").value = ""+file.name;
    document.getElementById("modol_form").submit();
}

function modalClose(){
    var fileName = document.getElementById("picture_name").value;
    document.getElementById("picture_name_input").value = fileName;
    document.getElementById("picture").src = "img/ap/"+fileName;
}

function submitForm(){
    var dropdown = document.getElementById('model_drop');
    var model_selected = dropdown.options[dropdown.selectedIndex].text;
    var eol = document.getElementById('eol_input').value;
    var eos = document.getElementById('eos_input').value;
    var picname = document.getElementById('picture_name_input').value;
    // alert("Model: "+model_selected+
    //         "\nEOL: "+eol+
    //         "\nEOS: "+eos+
    //         "\nPicname: "+picname
    // );
    $.ajax({
        type: "POST",
        url: "controller/phpfunction_switcher.php",
        data: {
                func: "update_database",
                model: model_selected,
                eol: eol,
                eos: eos,
                picname: picname     
        },
        success: function(data) {
            alert(data+" :)");
        },
        error: function(data) {
            alert("error "+data);
        }
    });
}

$(document).ready(function(){
    /* create and eostable in page loaded */
    $('#eoleos_table').DataTable({
        "order": [[ 1, "asc" ]],
        "pageLength": 25,
        "autoWidth": false
    });
});

$(document).ready(function(){
    /* create and locationtable in page loaded */
    $('#locationTable').DataTable({
        "order": [[ 4, "desc" ]],
        "pageLength": 10,
        "autoWidth": false
    });
});

$(document).ready(function(){
    /* create and iptable in page loaded */
    $('#iptable').DataTable({
        "order": [[ 1, "asc" ]],
        "pageLength": 25,
        destroy: true,
        "autoWidth": false
    });
});
